#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Nov 28 18:34:33 2017

@author: nuno
"""

import pandas as pd
import numpy as np
import math
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import axes3d
from sklearn.cluster import KMeans
from sklearn.cluster import DBSCAN
from sklearn.mixture import GaussianMixture
from sklearn.neighbors import KNeighborsClassifier
from sklearn.metrics import silhouette_score
from sklearn.metrics import adjusted_rand_score
from scipy.misc import imread



data = pd.read_csv('tp2_data.csv')

dt=data[['latitude', 'longitude', 'fault']]

Radius=6371
X=Radius*(np.cos(dt['latitude']*math.pi/180)*np.cos(dt['longitude']*math.pi/180))
Y=Radius*(np.cos(dt['latitude']*math.pi/180)*np.sin(dt['longitude']*math.pi/180))
Z=Radius*(np.sin(dt['latitude']*math.pi/180))

fig=plt.figure()
ax = fig.add_subplot(111, projection='3d')
ax.scatter(X, Y, Z,s=10)
plt.show()
df=pd.DataFrame({'X':X, 'Y':Y, 'Z':Z})

def score(data,predict,alg,numClusters):
    TP = 0
    TN = 0
    FP = 0
    FN = 0
    N = np.sum(data != -1)  #number of points that count
    for i in range(0,len(predict)):
        if(data[i] != -1):
            for j in range(i + 1, len(predict)):
                if(data[j] != -1):
                    if(predict[i] == predict[j] and data[i] == data[j]):
                        TP+=1
                    if(predict[i] != predict[j] and data[i] != data[j]):
                        TN+=1
                    if(predict[i] == predict[j] and data[i] != data[j]):
                        FP+=1
                    if(predict[i] != predict[j] and data[i] == data[j]):
                        FN+=1
    col = ['Number of CLusters','Precision','Recall','Rand','F1','Silhouette', 'Adjusted Rand']
    print(TP+TN+FP+FN)
    print(((N*(N-1))/2))
    silhouette= silhouette_score(df, predict)
    adjustedRand = adjusted_rand_score(data,predict)
    precision = TP/(TP+FP)
    recall = TP/(TP+FN)
    rand = (TP + TN)/((N*(N-1))/2)
    f1 = 2*((precision*recall)/(precision+recall))
    result = pd.DataFrame.from_items([(alg, [numClusters,precision,recall,rand,f1,silhouette,adjustedRand])], orient='index', columns=col)
    return result

def plotInd(result,init, final,xrange):
    numClusters = result['Number of CLusters'].as_matrix()
    precision = result['Precision'].as_matrix()
    recall = result['Recall'].as_matrix()
    rand = result['Rand'].as_matrix()
    f1 = result['F1'].as_matrix()
    silhouette = result['Silhouette'].as_matrix()
    adjusted = result['Adjusted Rand'].as_matrix()

    print(numClusters)
    plt.figure(figsize=(13,8), frameon= None)

    plt.plot(numClusters,precision,'-',label="Precision",linewidth=4)
    plt.plot(numClusters,recall,'-',label="Recall",linewidth=4)
    plt.plot(numClusters,rand,'-',label="Rand",linewidth=4)
    plt.plot(numClusters,f1,'-',label="F1",linewidth=4)
    plt.plot(numClusters,silhouette,'-',label="Silhouette",linewidth=4)
    plt.plot(numClusters,adjusted,'-',label="Adj Rand",linewidth=4)
    plt.xticks(np.arange(init,final,xrange))
    plt.grid(b=True, which='major', axis='both')

    plt.legend(bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0.)


    plt.show()
    plt.savefig('chart.png',dpi=300)

#kmeans
def kMeans():
    #n=np.unique(dt['fault']).size*5
    n=40
    frames=[]
    initK = i= 3
    while i <= n:
        kmeans = KMeans(n_clusters=i).fit(df)
        clusters = kmeans.predict(df)
        faultLine = dt['fault'].as_matrix()
        frames.append(score(faultLine,clusters,'K-Means',i))
        i+=2

    result = pd.concat(frames)
    plotInd(result,initK, n,1)
    #plot_classes(clusters,dt['longitude'],dt['latitude'])


#DBSCAN
def dbscan():
    kneighbors=KNeighborsClassifier(n_neighbors=4)
    kneighbors.fit(df,np.zeros(len(dt['fault'])))
    dist, lixo=kneighbors.kneighbors()
    frames=[]
    dist=dist[:,3]
    dist=np.flip(np.sort(dist,0),0)
    
    plt.figure(figsize=(13,8), frameon= None)
    plt.plot(list(range(1,3882)),dist)
    plt.yticks(np.arange(0,max(dist),100))
    plt.xticks(np.arange(0,3882,200))
    plt.grid(b=True, which='major', axis='both')
    plt.show()
    
    initE = i=100
    finalE = 500
    while i<=finalE:
        #dbscan=DBSCAN(eps=dist[i]).fit(df)
        dbscan=DBSCAN(eps=i).fit(df)
        clusters=dbscan.fit_predict(df)
        print(clusters)
        faultLine = dt['fault'].as_matrix()
        #frames.append(score(faultLine,clusters,'DBCSAN',int(round(dist[i]))))
        frames.append(score(faultLine,clusters,'DBCSAN',i))
        i=i+20
    result = pd.concat(frames)

    #plotInd(result,int(round(dist[finalE])),int(round(dist[initE])),1)    
    plotInd(result,initE,finalE,20)    
    #plot_classes(clusters,dt['longitude'],dt['latitude'])

    


#GMM
def gmm():
    #n=np.unique(dt['fault']).size+15
    n=40
    frames=[]
    initC = i= 3
    while i <= n:
        gmm = GaussianMixture(n_components=i).fit(df)
        clusters = gmm.predict(df)
        print(clusters)
        faultLine = dt['fault'].as_matrix()
        frames.append(score(faultLine,clusters,'GMM',i))
        i+=2

    result = pd.concat(frames)
    plotInd(result,initC, n,2)
    #plot_classes(clusters,dt['longitude'],dt['latitude'])

    

def plot_classes(labels,lon,lat, alpha=0.5, edge = 'k'):
    """Plot seismic events using Mollweide projection.
    Arguments are the cluster labels and the longitude and latitude
    vectors of the events"""
    img = imread("Mollweide_projection_SW.jpg")
    plt.figure(figsize=(10,5),frameon=False)
    x = lon/180*np.pi
    y = lat/180*np.pi
    ax = plt.subplot(111, projection="mollweide")
    print(ax.get_xlim(), ax.get_ylim())
    t = ax.transData.transform(np.vstack((x,y)).T)
    print(np.min(np.vstack((x,y)).T,axis=0))
    print(np.min(t,axis=0))
    clims = np.array([(-np.pi,0),(np.pi,0),(0,-np.pi/2),(0,np.pi/2)])
    lims = ax.transData.transform(clims)
    plt.close()
    plt.figure(figsize=(10,5),frameon=False)
    plt.subplot(111)
    plt.imshow(img,zorder=0,extent=[lims[0,0],lims[1,0],lims[2,1],lims[3,1]],aspect=1)
    x = t[:,0]
    y= t[:,1]
    nots = np.zeros(len(labels)).astype(bool)
    diffs = np.unique(labels)
    ix = 0
    for lab in diffs[diffs>=0]:
        mask = labels==lab
        nots = np.logical_or(nots,mask)
        plt.plot(x[mask], y[mask],'o', markersize=4, mew=1,zorder=1,alpha=alpha, markeredgecolor=edge)
        ix = ix+1
    mask = np.logical_not(nots)
    if np.sum(mask)>0:
        plt.plot(x[mask], y[mask], '.', markersize=1, mew=1,markerfacecolor='w', markeredgecolor=edge)
    plt.axis('off')


#plot_classes(dt['fault'],dt['longitude'],dt['latitude'])

kMeans()
dbscan()
gmm()


